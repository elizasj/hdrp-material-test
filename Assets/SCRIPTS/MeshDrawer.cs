using System.Collections.Generic;
using UnityEngine;

public class MeshDrawer : MonoBehaviour
{
    public GameObject cubePrefab;

    private Mesh faceMesh;
    private Vector3[] vertices;
    private int[] triangles;

    private Matrix4x4 translationMatrix = Matrix4x4.identity;
 

    // Structure to hold the association between a cube and a vertex index
    private struct CubeVertexAssociation
    {
        public GameObject cube;
        public int vertexIndex;
    }

    // List to track all cube-vertex associations
    private List<CubeVertexAssociation> cubeVertexAssociations = new List<CubeVertexAssociation>();

    private void Start()
    {
        // Get the mesh data of the face mesh
        faceMesh = GetComponent<MeshFilter>().mesh;
        vertices = faceMesh.vertices;
        triangles = faceMesh.triangles;
        Debug.Log("Start: Mesh data loaded.");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Cast a ray from the camera towards the mouse position
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 2f);

            // Check if the ray intersects the mesh and find the intersection point
            Vector3? intersectionPoint = RayIntersectsMesh(ray);
            if (intersectionPoint.HasValue)
            {
                // Log the closest vertex to the intersection point
                Debug.Log("Update: Clicked vertex near " + intersectionPoint.Value);
            }
            else
            {
                Debug.Log("Update: Clicked outside the face mesh.");
            }
        }
    }

    // Add the following method to your class
    private void LateUpdate()
    {
        UpdateCubePositions();
    }

    private void UpdateCubePositions()
    {
        foreach (var association in cubeVertexAssociations)
        {
            // Convert the vertex's local position to world position
            Vector3 currentVertexPosition = transform.TransformPoint(faceMesh.vertices[association.vertexIndex]);
            Quaternion currentRotation = Quaternion.LookRotation(translationMatrix.GetColumn(2), translationMatrix.GetColumn(1));
            Vector3 eulerRotation = currentRotation.eulerAngles;
            Quaternion newAxisRotation = Quaternion.Euler(-eulerRotation.y, eulerRotation.x, eulerRotation.z);

            // Update the cube's position to match the vertex's world position
            association.cube.transform.position = currentVertexPosition;
            association.cube.transform.rotation = newAxisRotation;

            //association.cube.transform.localScale = new Vector3(x, y, z);//0.033333f * (new Vector3(2f, 2f, 2f) -  scale);

            //association.cube.transform.rotation = currentRotation* rotationEuler;
        }
    }

    private Vector3? RayIntersectsMesh(Ray ray)
    {

        // Fetch the latest mesh data.
        faceMesh = GetComponent<MeshFilter>().mesh; // Refresh the reference
        vertices = faceMesh.vertices; // Get the latest vertices
        triangles = faceMesh.triangles; // Get the latest triangles

        Debug.Log("RayIntersectsMesh: Ray casting started.");
        float closestDistance = float.MaxValue;
        Vector3? closestPoint = null;

        // Convert vertices from local space to world space
        Vector3[] worldVertices = new Vector3[vertices.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            worldVertices[i] = transform.TransformPoint(vertices[i]);
        }

        // Implement a simple ray-triangle intersection test
        for (int i = 0; i < triangles.Length; i += 3)
        {
            Vector3 v0 = worldVertices[triangles[i]];
            Vector3 v1 = worldVertices[triangles[i + 1]];
            Vector3 v2 = worldVertices[triangles[i + 2]];

            Debug.Log($"RayIntersectsMesh: Checking triangle {i / 3} ({v0}, {v1}, {v2})");

            if (RayIntersectsTriangle(ray, v0, v1, v2, out Vector3 intersection))
            {
                Debug.Log($"RayIntersectsMesh: Intersection found at {intersection}");

                float distance = Vector3.Distance(ray.origin, intersection);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestPoint = intersection;
                }
            }
        }

        if (closestPoint.HasValue)
        {
            Debug.Log($"RayIntersectsMesh: Closest intersection point: {closestPoint.Value}");
            GameObject cubeInstance = Instantiate(cubePrefab, closestPoint.Value, Quaternion.identity);
            cubeInstance.transform.parent = this.transform; // Set parent to face mesh

            // Find the closest vertex index to the intersection point
            int closestVertexIndex = FindClosestVertexIndex(closestPoint.Value);
            cubeVertexAssociations.Add(new CubeVertexAssociation { cube = cubeInstance, vertexIndex = closestVertexIndex });
        }

        else
        {
            Debug.Log("RayIntersectsMesh: No intersection point found.");
        }

        return closestPoint;
    }

    private bool RayIntersectsTriangle(Ray ray, Vector3 v0, Vector3 v1, Vector3 v2, out Vector3 intersection)
    {
        // Initial logging to confirm the method is called with correct values
        Debug.Log($"RayIntersectsTriangle: Testing triangle with vertices {v0}, {v1}, {v2}");

        intersection = Vector3.zero;
        Vector3 edge1 = v1 - v0;
        Vector3 edge2 = v2 - v0;
        Vector3 h = Vector3.Cross(ray.direction, edge2);
        float a = Vector3.Dot(edge1, h);

        // Log when the ray is parallel to the triangle plane
        if (Mathf.Abs(a) < 0.0001f)
        {
            Debug.Log("RayIntersectsTriangle: Ray is parallel to triangle plane.");
            return false;
        }

        float f = 1.0f / a;
        Vector3 s = ray.origin - v0;
        float u = f * Vector3.Dot(s, h);

        // Log potential issues with u parameter
        if (u < 0.0 || u > 1.0)
        {
            Debug.Log($"RayIntersectsTriangle: u parameter out of bounds, u={u}");
            return false;
        }

        Vector3 q = Vector3.Cross(s, edge1);
        float v = f * Vector3.Dot(ray.direction, q);

        // Log potential issues with v parameter
        if (v < 0.0 || u + v > 1.0)
        {
            Debug.Log($"RayIntersectsTriangle: v parameter out of bounds, v={v}, u+v={u + v}");
            return false;
        }

        // Calculate where the intersection point is on the line
        float t = f * Vector3.Dot(edge2, q);

        // Check if the intersection is in the correct direction and log the intersection point
        if (t > 0.0001f) // ray intersection
        {
            intersection = ray.origin + ray.direction * t;
            Debug.Log($"RayIntersectsTriangle: Intersection at {intersection}, t={t}");
            return true;
        }
        else
        {
            Debug.Log("RayIntersectsTriangle: Intersection point is behind the ray origin.");
            return false;
        }
    }

    private int FindClosestVertexIndex(Vector3 point)
    {
        int closestIndex = 0;
        float closestDistance = float.MaxValue;

        for (int i = 0; i < vertices.Length; i++)
        {
            float distance = Vector3.Distance(transform.TransformPoint(vertices[i]), point);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestIndex = i;
            }
        }

        return closestIndex;
    }

    public void ApplyFaceDataToObjects(Matrix4x4 faceMatrix)
    {
        
        translationMatrix = faceMatrix;
    

    }


}

/* Copyright (c) 2024 dr. ext (Vladimir Sigalkin) */

using UnityEngine;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;

namespace extOSC
{
    public class GetFaceData : MonoBehaviour
    {
        #region Public Vars

        [Header("OSC Settings")]
        public OSCReceiver Receiver;
        
        [Header("Link to Face Mesh")]
        public GetMeshPoints meshDeformer; // link to GetMeshPoints.cs
        public MeshDrawer rotObjects; // link to MeshDrawer.cs

        #endregion

        #region Private Vars

        private const string _address = "/getFaceData";
        private Quaternion rotation = Quaternion.identity;
        private Vector3 euler;
        private float[] floatArray = null;

        #endregion

        #region Unity Methods

        protected virtual void Start()
        {
            // Register receive callback.
            Receiver.Bind(_address, MessageReceived);

        }

        #endregion

        #region Protected Methods

        protected void MessageReceived(OSCMessage message)
        {
            if (message != null && message.Values.Count > 0 && message.Values[0].Type == OSCValueType.Array)
            {

                // FACE POINTS
                var faceDataOSCValue = message.Values[0]; // This is the first element of the OSC message
            

                List<float> allFaceData = new List<float>(); // List to collect all float values

                // Iterating over each item in the first element, expecting a sub-array of floats
                foreach (var subArrayValue in faceDataOSCValue.ArrayValue)
                {
                    if (subArrayValue.Type == OSCValueType.Array)
                    {
                        var nestedArray = subArrayValue.ArrayValue; // Accessing the nested array -- floatSubArray

                        foreach (var oscValue in nestedArray)
                        {
                            if (oscValue.Type == OSCValueType.Float)
                            {
                                allFaceData.Add(oscValue.FloatValue); // Collecting the float value
                            }
                            else
                            {
                                Debug.Log($"Expected float, found {oscValue.Type}");
                            }
                        }
                    }
                    else
                    {
                        Debug.Log($"Expected OSCValueType.Array, found {subArrayValue.Type}");
                    }
                }

                //Debug.Log($"Collected {allFloats.Count} float values from faceData.");

                // Now you have all the float values in allFloats list
                if (allFaceData.Count > 0)
                {
                    // Convert the list of all float values to an array
                    floatArray = allFaceData.ToArray();
                }
                else
                {
                    Debug.Log("No float values were collected from faceData.");
                }


                // ROTATION VALUES
                var transformationMatricesOSCValue = message.Values[1];
               // Debug.Log(transformationMatricesOSCValue.ToString());

                if (transformationMatricesOSCValue.Type == OSCValueType.Array && transformationMatricesOSCValue.ArrayValue.Count == 1)
                {
                    var oscMatrixArrayValue = transformationMatricesOSCValue.ArrayValue[0];
                    if (oscMatrixArrayValue.Type == OSCValueType.Array && oscMatrixArrayValue.ArrayValue.Count == 16)
                    {
                        Matrix4x4 matrix = new Matrix4x4();

                        Vector4 column0 = new Vector4(oscMatrixArrayValue.ArrayValue[0].FloatValue, oscMatrixArrayValue.ArrayValue[1].FloatValue, oscMatrixArrayValue.ArrayValue[2].FloatValue, 0f);
                        Vector4 column1 = new Vector4(oscMatrixArrayValue.ArrayValue[4].FloatValue, oscMatrixArrayValue.ArrayValue[5].FloatValue, oscMatrixArrayValue.ArrayValue[6].FloatValue, 0f);
                        Vector4 column2 = new Vector4(oscMatrixArrayValue.ArrayValue[8].FloatValue, oscMatrixArrayValue.ArrayValue[9].FloatValue, oscMatrixArrayValue.ArrayValue[10].FloatValue, 0f);
                        Vector4 column3 = new Vector4(oscMatrixArrayValue.ArrayValue[12].FloatValue, oscMatrixArrayValue.ArrayValue[13].FloatValue, oscMatrixArrayValue.ArrayValue[14].FloatValue, 1f);


                        matrix.SetColumn(0, column0);
                        matrix.SetColumn(1, column1);
                        matrix.SetColumn(2, column2);
                        matrix.SetColumn(3, column3);

                        meshDeformer.ApplyFaceDataToMesh(floatArray);
                        rotObjects.ApplyFaceDataToObjects(matrix);


                    }
                    else
                    {
                        Debug.LogWarning("Expected 16 elements in the OSC matrix array, but got a different count.");
                    }
                }
                else
                {
                    Debug.LogWarning("Expected an array type for transformation matrices, found something else.");
                }



            }

            else
            {
                Debug.Log("Received message is null or not in the expected format.");
            }
        }

        #endregion
    }
}
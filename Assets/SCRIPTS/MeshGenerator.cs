using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class MeshGenerator : MonoBehaviour
{
    // Use TextAsset to allow for easy assignment in Unity's Inspector
    public TextAsset dataTextAsset;

    void Start()
    {
        if (dataTextAsset != null)
        {
            GenerateMeshFromDataText();
        }
        else
        {
            Debug.LogError("Data text asset is not assigned.");
        }
    }

    void GenerateMeshFromDataText()
    {
        // Split the file content by new lines to get each line as a separate string
        string[] lines = dataTextAsset.text.Split('\n');

        List<Vector3> vertices = new List<Vector3>();
        foreach (var line in lines)
        {
            // Split each line by tabs or spaces to get individual components
            string[] components = line.Split(new[] { '\t', ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
            if (components.Length >= 3)
            {
                // Try to parse each component as an integer and then convert to a Vector3
                if (float.TryParse(components[0], out float x) &&
                    float.TryParse(components[1], out float y) &&
                    float.TryParse(components[2], out float z))
                {
                    vertices.Add(new Vector3(x, y, z));
                }
            }
        }

        if (vertices.Count > 0)
        {
            CreateMesh(vertices);
        }
        else
        {
            Debug.LogError("No vertices parsed from the data.");
        }
    }

    void CreateMesh(List<Vector3> vertices)
    {
        // Create a new Mesh
        Mesh mesh = new Mesh();

        // Assign vertices
        mesh.vertices = vertices.ToArray();

        // Automatically create triangles - this is simplistic and might need adjustments
        List<int> triangles = new List<int>();
        for (int i = 0; i < vertices.Count; i += 3)
        {
            if (i + 2 < vertices.Count) // Ensure there's enough vertices to form a triangle
            {
                triangles.Add(i);
                triangles.Add(i + 1);
                triangles.Add(i + 2);
            }
        }

        mesh.triangles = triangles.ToArray();

        // Recalculate normals for proper lighting
        mesh.RecalculateNormals();

        // Optionally, you can set up UVs, tangents, etc. here

        // Assign the mesh to a MeshFilter or MeshRenderer component
        GetComponent<MeshFilter>().mesh = mesh;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetMeshPoints : MonoBehaviour
{

    #region Public Vars

    [Header("Mesh")]
    public MeshFilter targetMesh;
    public GameObject parent;
    public GameObject child;
    #endregion

    #region Private Vars

    private Mesh mesh;

    private Vector3[] vertices;
    private Vector3[] ogVertices;

    private Vector3[] modVertices;

    private float[] latestFaceData = null;
    private bool hasNewFaceData = false;

    #endregion

    #region Unity Methods

    // Start is called before the first frame update
    void Start()
    {
    
        if (targetMesh != null)
        {
            // Directly access the mesh
            mesh = targetMesh.mesh;
            //Debug.Log("this mesh has " + mesh.vertexCount.ToString() + " vertices"); // facemesh is 483 vertices

            ogVertices = mesh.vertices;
            modVertices = mesh.vertices;

            vertices = mesh.vertices;
        }
        else
        {
            Debug.LogWarning("MeshFilter is not assigned.");
        }

    }

    // Update is called once per frame
    void Update()
    {
        // Copy the original vertex to maintain the rest of the mesh shape
        // to ensure that you're working with a fresh, unmodified set of
        // vertex positions every time you apply a change. 
        modVertices = (Vector3[])ogVertices.Clone();

        // update all the face points
        if (hasNewFaceData)
        {
            // Assuming latestFaceData is correctly sized for the mesh
            if (latestFaceData.Length - 30 == ogVertices.Length * 3)
            {
                // Prepare to update mesh vertices
                Vector3[] updatedVertices = new Vector3[ogVertices.Length];
                for (int i = 0, j = 0; i < latestFaceData.Length - 30; i += 3, j++)
                {
                    // Convert each set of three floats into a Vector3 vertex
                    updatedVertices[j] = new Vector3(latestFaceData[i], latestFaceData[i + 1], latestFaceData[i + 2]);
                    //updatedVertices[j] = translationMatrix.MultiplyPoint3x4(new Vector3(latestFaceData[i], latestFaceData[i + 1], latestFaceData[i + 2]));

                }

                // Apply updated vertices to the mesh
                mesh.vertices = updatedVertices;
                mesh.RecalculateBounds();

                //child.transform.localRotation = Quaternion.identity;
                //parent.transform.rotation = latestFaceRotationData;

                hasNewFaceData = false; // Reset the flag
            }
            else
            {
                Debug.LogWarning("Mismatch between received face data points and mesh vertices.");
            } 
            //Debug.Log(latestFaceRotationData);
        }

        //parent.transform.localScale = translationMatrix.ExtractScale();
       // parent.transform.rotation = translationMatrix.ExtractRotation();
       // parent.transform.position = translationMatrix.ExtractPosition();

        //child.transform.rotation = Quaternion.LookRotation(translationMatrix.GetColumn(2), translationMatrix.GetColumn(1));
        //child.transform.position = translationMatrix.GetColumn(3);

    }

    #endregion

    public void ApplyFaceDataToMesh(float[] faceDataPoints)
    {
        if (ogVertices == null || faceDataPoints.Length % 3 != 0)
            return; // Ensure the data points are complete (x, y, z) sets

        //Debug.Log(faceDataPoints.Length); // 478 in td -> 1434 ... 483�3=1449

        latestFaceData = faceDataPoints; // Store the latest face data
        hasNewFaceData = true; // Signal that new face data has arrived

    }


}



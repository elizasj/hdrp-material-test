using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderQueueChanger : MonoBehaviour
{
    public int renderQueue = 2950; // Custom render queue, set to just before the transparent queue

    void Start()
    {
        Renderer renderer = GetComponent<Renderer>();
        if (renderer)
        {
            Material material = renderer.material;
            if (material)
            {
                material.renderQueue = renderQueue;
            }
        }
    }
}
